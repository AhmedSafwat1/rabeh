<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    //
    // user have
    public function User()
    {
        return $this->belongsTo('App\User');
    }
    // Service have
    public function Service()
    {
        return $this->belongsTo('App\Service');
    }
}

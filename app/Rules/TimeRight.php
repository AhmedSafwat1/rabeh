<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;
class TimeRight implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public $lang="";
    public function __construct($lang)
    {
        //
        $this->lang = $lang;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $dt      = Carbon::now();
        $h       = $dt->hour  ;
        $m       = $dt->minute;
        $enter   = Carbon::parse($value);
        $eh      = $enter->hour;
        $em      = $enter->minute;
        return $eh > $h     || ($eh == $h && $em >= $m) ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->lang == "ar" ? "الوقت المدخل غير صحيح لانه قد انتهى ":'Time which enter not correct beacue in the previous.';
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['device_id', 'device_type', 'user_id'];
    // user have
    public function User()
    {
        return $this->belongsTo('App\User');
    }
}

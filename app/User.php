<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','mobile','code','device_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role()
    {
        return $this->hasOne('App\Role','id','role');
    }

    public function Reports()
    {
        return $this->hasMany('App\Report','user_id','id');
    }

    //Rate Given
    public function RatesGiven()
    {
        return $this->hasMany('App\Rate',"user_id","id");
    }
    //Rate Owner
    public function RatesOwner()
    {
        return $this->hasMany('App\Rate',"provider_id","id");
    }

    //device Owner
    public function Devices()
    {
        return $this->hasMany('App\Device');
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }
    public function getJWTCustomClaims() {
        return [];
    }

}

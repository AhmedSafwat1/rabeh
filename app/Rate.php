<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //
    // user make
    public function User()
    {
        return $this->belongsTo('App\User',"user_id", "id");
    }
    // Provider make
    public function Provider()
    {
        return $this->belongsTo('App\User',"provider_id" ,"id");
    }
}

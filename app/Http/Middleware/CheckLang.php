<?php

namespace App\Http\Middleware;

use Closure;
use App;
class CheckLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if(isset($request["lang"]))
        {

            $request["old_lang"] = App::getLocale();
            App::setLocale($request["lang"]);
            return $next($request);
        }
        else
        {
            return response()->json(['key'=>'fail','value'=>'0','msg'=>"lang required"]);
        }

    }
    public function terminate($request, $response)
    {
        App::setLocale($request["old_lang"]);

    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DailyUpdateService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'chek if the service expire will make status 0 refuse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //clean service
        cleanService();
        $this->info(' Update has been send successfully');
    }
}

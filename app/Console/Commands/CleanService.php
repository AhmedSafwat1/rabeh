<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CleanService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete service which refuse and expire date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        cleanService(1,0);
        $this->info('delete finsh done');
    }
}

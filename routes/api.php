<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


//** start user controller **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        // sign in route
        Route::post('/sign-up'               ,'UsersController@signUp');
        //login
        Route::post('/sign-in'               ,'UsersController@signIn');
        //logout
        Route::post('/logout'               ,'UsersController@logout');
        //update profile
        Route::post('/update-profile'       ,'UsersController@updateProfile');
        //change password
        Route::post('/change_password'       ,'UsersController@changePassword');
        //Rate
        Route::post('/rate_action'          ,'UsersController@postRate');
        //get user
        Route::post('/get_user'             ,'UsersController@getUser');
        //chnage langue
        Route::post('/chnage_lang'          ,'UsersController@ChnageLang');
        //complation
        Route::post('/complation'          ,'UsersController@complation');
        //contact us
        Route::post('/contact-us'          ,'UsersController@ContactUs');
        //forget password
        Route::post('/forget-password'      ,'UsersController@forgotPassword');
        //forget password
        Route::post('/reset-password'      ,'UsersController@resetPassword');
        //resend password
        Route::post('/resend-code'         ,'UsersController@ResendCode');
    });

});
//end user controller
//** start basicinfoControler **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        //copy right
        Route::any('/terms'             ,'BasicInfoController@GetCopyRight');
        //copy desc
        Route::any('/about'            ,'BasicInfoController@aboutUs');
        // get transpernat
        Route::any('/get_transports'       ,'BasicInfoController@GetTransports');


    });

});
//end user basicinfoControler

//** start ChatControler **//
Route::group( ['namespace' => 'API'], function() {
    //route need to lang
    Route::group( ['middleware' => ['lang']], function() {
        //save
        Route::post('/send-message'         ,'ChatController@saveMessage');
        // get all mesage in chat
        Route::post('/chat-messages'        ,'ChatController@getChatMessages');
        // delete chat
        Route::post('/delete-chat'          ,'ChatController@deleteChatRom');
        // get chats
        Route::post('/get-chats'           ,'ChatController@getChatRoms');
        // chat message
        // get chats
        Route::post('/get-allMessags'      ,'ChatController@getChatMessagesById');

    });

});
//end user ChatControler

//** Start SettingController**//

//** End SettingController**//

Route::group(['middleware' => ['mobile'] , 'namespace' => 'API'], function() {

    Route::any('edit-profile'            ,'AuthController@editProfile');
    Route::any('edit-password'           ,'AuthController@editPassword');

});